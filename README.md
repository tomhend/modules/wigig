# Module for simulating 802.11ad/ay (WiGig) devices in ns-3.

This is the completion of work previously
[started by Sebastien Deronne](https://www.nsnam.org/wp-content/uploads/2021/wigig-report-sept-2021.pdf)
and recently finished by Brian Swenson.
The [original wigig module](https://github.com/wigig-tools/wigig-module) was
created by Hany Assasa and IMDEA, but it contained a lot of code duplicated
from wifi module (which Sebastien has removed).

Install this module into the `contrib` directory of ns-3 (3.39 release or
later).
